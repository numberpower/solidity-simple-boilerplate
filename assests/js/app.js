if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
}

web3.eth.defaultAccount = web3.eth.accounts[0];

var secretsKeeperABI = [{
        "constant": false,
        "inputs": [{
                "name": "_password",
                "type": "string"
            },
            {
                "name": "_secret",
                "type": "string"
            }
        ],
        "name": "keepSecret",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [{
            "name": "_password",
            "type": "string"
        }],
        "name": "tellSecret",
        "outputs": [{
            "name": "",
            "type": "string"
        }],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    }
];
var secretsKeeperADDRESS = '0xb3e40f40f50acfadef9a129fdc37bed8836e6de8';

var secretsKeeperCONTRACT = web3.eth.contract(secretsKeeperABI);
var secretsKeeper = secretsKeeperCONTRACT.at(secretsKeeperADDRESS);
console.log(secretsKeeper);

function getSecret(_pass) {
    secretsKeeper.tellSecret(_pass, function(error, result) {
        if (!error) {
            console.log(result + " 123");
            $("#secret").html(result);
        } else
            console.error(error);
    });
}

function getSecret(_pass) {
    secretsKeeper.tellSecret(_pass, function(error, result) {
        if (!error) {
            console.log(result + " 123");
            $("#secret").html(result);
        } else
            console.error(error);
    });
}


function keepSecret(_password, _secret) {
    secretsKeeper.keepSecret(_password, _secret, function(error, result) {
        if (!error) {
            console.log(result + " 123");
        } else
            console.error(error);
    });
}


$("#getBtn").on("click", function() {
    var pass = $("#getSecret").val();
    getSecret(pass);
});
$("#keepBtn").on("click", function() {
    console.log("fo2");
    var password = $("#password").val();
    var secret = $("#keepSecret").val();
    keepSecret(password, secret);
    console.log("ta7t"); //012345
});

$("#getBtn").on("click", function() {
    var pass = $("#getSecret").val();
    getSecret(pass);
});
$("#keepBtn").on("click", function() {
    console.log("fo2");
    var password = $("#password").val();
    var secret = $("#keepSecret").val();
    keepSecret(password, secret);
    console.log("ta7t"); //012345
});